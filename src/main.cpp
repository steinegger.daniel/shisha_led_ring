#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define POT_PIN 3
#define LED_PIN 7
#define NUMBER_OF_PIXELS 24
// #define RESOLUTION 1023

float oldPotiValue = -1;

unsigned long fadeTimestamp = 0;
unsigned int fadeSpeed = 100;
unsigned int fadeCounter = 0;

Adafruit_NeoPixel pixels(NUMBER_OF_PIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);

// float hue2rgb(float p, float q, float t)
// {
//   if (t < 0.0)
//   {
//     t += 1.0;
//   }
//   else if (t > 1.0)
//   {
//     t -= 1.0;
//   }

//   if (t < 1.0 / 6.0)
//   {
//     return p + (q - p) * 6.0 * t;
//   }
//   else if (t < 1.0 / 2.0)
//   {
//     return q;
//   }
//   else if (t < 2.0 / 3.0)
//   {
//     return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
//   }
//   return p;
// }

// void hslToRgb(byte *rgb, float h)
// {
//   float r, g, b;
//   h /= 360.0;
//   float s = 1.0;
//   float l = 0.5;

//   if (s == 0.0)
//   {
//     r = l;
//     g = l;
//     b = l; // achromatic
//   }
//   else
//   {
//     float q = (l < 0.5) ? (l * (1.0 + s)) : (l + s - l * s);
//     float p = 2.0 * l - q;
//     r = hue2rgb(p, q, h + 1.0 / 3.0);
//     g = hue2rgb(p, q, h);
//     b = hue2rgb(p, q, h - 1.0 / 3.0);
//   }

//   rgb[0] = r * 255.0 + 0.5;
//   rgb[1] = g * 255.0 + 0.5;
//   rgb[2] = b * 255.0 + 0.5;
// }

uint32_t Wheel(byte WheelPos)
{
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85)
  {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170)
  {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void printHeader(String text)
{
  Serial.println("----------");
  Serial.println("-- " + text);
  Serial.println("----------");
}

void setup()
{
  Serial.begin(9600);
  delay(1000);
  printHeader("Start Setup");
  // initialize digital pin LED_BUILTIN as an output.

  pinMode(POT_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
  pixels.begin();
  pixels.setBrightness(50);
  pixels.show();
  // put your setup code here, to run once:
}

void loop()
{

  // for (int i = 0; i < NUMBER_OF_PIXELS; i++)
  // {
  //   pixels.setPixelColor(i, Wheel(fadeCounter));
  // }
  // pixels.show();

  // fadeCounter += 1.0;

  // delay(250);

  const int potiValue = round(analogRead(POT_PIN) / 1023.0 * 255.0);

  if (potiValue < 255)
  {
    Serial.print("POTI: ");
    Serial.println(potiValue);
    if (potiValue != oldPotiValue)
    {
      for (int i = 0; i < NUMBER_OF_PIXELS; i++)
      {
        pixels.setPixelColor(i, Wheel(potiValue));
      }
      pixels.show();
      oldPotiValue = potiValue;
      fadeCounter = potiValue;
    }
  }
  else
  {

    if ((unsigned long)(millis() - fadeTimestamp) >= fadeSpeed)
    {
      Serial.println("FADE");
      for (int i = 0; i < NUMBER_OF_PIXELS; i++)
      {
        pixels.setPixelColor(i, Wheel(fadeCounter));
      }
      pixels.show();
      fadeTimestamp = millis();
      fadeCounter = (fadeCounter + 1) % 255;
    }
  }
  delay(50);
}